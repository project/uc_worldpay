<?php
/**
 * @file
 * Install/uninstall and update functions for the uc_worldpay module.
 */

/**
 * Implements hook_schema().
 */
function uc_worldpay_schema() {
  $schema = array();

  $schema['uc_payment_worldpay'] = array(
    'description' => 'Logs WorldPay payment response parameters.',
    'fields' => array(
      'order_id' => array(
        'description' => 'The order ID.',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ),
      'transid' => array(
        'description' => 'The transaction ID from WorldPay.',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => '',
      ),
      'cardtype' => array(
        'description' => 'The type of payment method used by the shopper. E.g. Visa.',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => '',
      ),
      'transtime' => array(
        'description' => 'Time of the transaction in milliseconds since the start of 1970 GMT.',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => '',
      ),
      'futurepayid' => array(
        'description' => 'The future pay ID from WorldPay.',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => '',
      ),
      'authamount' => array(
        'description' => 'Authorized amount.',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => '',
      ),
      'authcurrency' => array(
        'description' => 'Currency for authorized amount.',
        'type' => 'varchar',
        'length' => 10,
        'not null' => TRUE,
        'default' => '',
      ),
      'authmessage' => array(
        'description' => 'Authorization message from bank.',
        'type' => 'varchar',
        'length' => 2047,
        'not null' => TRUE,
        'default' => '',
      ),
      'authcode' => array(
        'description' => 'Authorization code from bank.',
        'type' => 'varchar',
        'length' => 10,
        'not null' => TRUE,
        'default' => '',
      ),
      'transstatus' => array(
        'description' => 'Transaction status, Y or C.',
        'type' => 'varchar',
        'length' => 10,
        'not null' => TRUE,
        'default' => '',
      ),
      'countrymatch' => array(
        'description' => 'Country match result by WorldPay.',
        'type' => 'varchar',
        'length' => 10,
        'not null' => TRUE,
        'default' => '',
      ),
      'avs' => array(
        'description' => 'Address verification result by WorldPay.',
        'type' => 'varchar',
        'length' => 10,
        'not null' => TRUE,
        'default' => '',
      ),
      'profile' => array(
        'description' => 'Payment profile name.',
        'type' => 'varchar',
        'length' => 100,
        'not null' => TRUE,
        'default' => '',
      ),
    ),
    'indexes' => array(
      'order_id' => array('order_id'),
    ),
    'foreign keys' => array(
      'order_id' => array('uc_orders' => 'order_id'),
    ),
  );

  return $schema;
}

/**
 * Implements hook_uninstall().
 */
function uc_worldpay_uninstall() {
  // Remove all uc_worldpay variables.
  db_delete('variable')
    ->condition('name', 'uc_worldpay%', 'LIKE')
    ->execute();
}

/**
 * Create the uc_payment_worldpay table if upgrade from Drupal 6.
 */
function uc_worldpay_update_7000() {
  $schema['uc_payment_worldpay'] = array(
    'description' => 'Logs WorldPay payment response parameters.',
    'fields' => array(
      'order_id' => array(
        'description' => 'The order ID.',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ),
      'transid' => array(
        'description' => 'The transaction ID from WorldPay.',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => '',
      ),
      'cardtype' => array(
        'description' => 'The type of payment method used by the shopper. E.g. Visa.',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => '',
      ),
      'transtime' => array(
        'description' => 'Time of the transaction in milliseconds since the start of 1970 GMT.',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => '',
      ),
    ),
    'indexes' => array(
      'order_id' => array('order_id'),
    ),
    'foreign keys' => array(
      'order_id' => array('uc_orders' => 'order_id'),
    ),
  );

  db_create_table('uc_payment_worldpay', $schema['uc_payment_worldpay']);
}

/**
 * Add additional table columns for payment status.
 */
function uc_worldpay_update_7010() {
  if (!db_field_exists('uc_payment_worldpay', 'transstatus')) {
    db_add_field('uc_payment_worldpay', 'transstatus', array(
        'description' => 'Transaction status, Y or C.',
        'type' => 'varchar',
        'length' => 10,
        'not null' => TRUE,
        'default' => '',
    ));
  }
  if (!db_field_exists('uc_payment_worldpay', 'futurepayid')) {
    db_add_field('uc_payment_worldpay', 'futurepayid', array(
        'description' => 'The future pay ID from WorldPay.',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => '',
    ));
  }
  if (!db_field_exists('uc_payment_worldpay', 'authamount')) {
    db_add_field('uc_payment_worldpay', 'authamount', array(
        'description' => 'Authorized amount.',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => '',
    ));
  }
  if (!db_field_exists('uc_payment_worldpay', 'authcurrency')) {
    db_add_field('uc_payment_worldpay', 'authcurrency', array(
        'description' => 'Currency for authorized amount.',
        'type' => 'varchar',
        'length' => 10,
        'not null' => TRUE,
        'default' => '',
    ));
  }
  if (!db_field_exists('uc_payment_worldpay', 'authmessage')) {
    db_add_field('uc_payment_worldpay', 'authmessage', array(
        'description' => 'Authorization message from bank.',
        'type' => 'varchar',
        'length' => 2047,
        'not null' => TRUE,
        'default' => '',
    ));
  }
  if (!db_field_exists('uc_payment_worldpay', 'authcode')) {
    db_add_field('uc_payment_worldpay', 'authcode', array(
        'description' => 'Authorization code from bank.',
        'type' => 'varchar',
        'length' => 10,
        'not null' => TRUE,
        'default' => '',
    ));
  }
  if (!db_field_exists('uc_payment_worldpay', 'countrymatch')) {
    db_add_field('uc_payment_worldpay', 'countrymatch', array(
        'description' => 'Country match result by WorldPay.',
        'type' => 'varchar',
        'length' => 10,
        'not null' => TRUE,
        'default' => '',
    ));
  }
  if (!db_field_exists('uc_payment_worldpay', 'avs')) {
    db_add_field('uc_payment_worldpay', 'avs', array(
        'description' => 'Address verification result by WorldPay.',
        'type' => 'varchar',
        'length' => 10,
        'not null' => TRUE,
        'default' => '',
    ));
  }
}

/**
 * Migrate configuration to allow multiple payment profiles.
 */
function uc_worldpay_update_7011(&$sandbox) {
  // Check to see if we have existing variables for the single Worldpay method. If so move them to the default.
  $sid = variable_get('uc_worldpay_sid', '');
  if (!empty($sid)) {
    $variables = array(
      'sid' => '',
      'merchant_code' => '',
      'debug' => 'log',
      'checkout_button' => 'Submit Order',
      'payment_methods' => array('visa', 'visa-electron', 'mastercard', 'masterpass', 'maestro'),
      'test' => TRUE,
      'test_result' => 'AUTHORISED',
      'md5' => '',
      'callbackPW' => '',
      'callbackURL' => '',
      'desc' => FALSE,
      'cancel_order' => FALSE,
      'edit_contact' => TRUE,
      'show_contact' => TRUE,
      'lang' => 'en-GB',
      'test_url' => 'https://secure-test.worldpay.com/wcc/purchase',
      'live_url' => 'https://secure.worldpay.com/wcc/purchase',
    );
    foreach ($variables as $variable => $value) {
      variable_set('uc_worldpay_worldpay_default_'. $variable, variable_get('uc_worldpay_'. $variable, $value));
      variable_del('uc_worldpay_'. $variable);
    }
  }
}

/**
 * Add payment profile to log table.
 */
function uc_worldpay_update_7012(&$sandbox) {
  // database update: add payment profile field  
  if (!db_field_exists('uc_payment_worldpay', 'profile')) {
    db_add_field('uc_payment_worldpay', 'profile', array(
        'description' => 'Payment profile name.',
        'type' => 'varchar',
        'length' => 100,
        'not null' => TRUE,
        'default' => '',
    ));
  }
}